jQuery(document).ready(function($){

    var anchor = window.location.hash;
    var collapseTitle = $('.field--name-field-collapsible-title');

    $('.field--name-field-collapsible-title, .hfc-open-all, .hfc-close-all').attr('role','button');
    $('.field--name-field-collapsible-title, .hfc-open-all, .hfc-close-all').attr('tabindex','0');
    $('.field--name-field-collapsible-title').attr('aria-expanded','false');
    $(collapseTitle).addClass('expand-plus');
    var openCloseButtons = $('<div class="collapsible-buttons"><span class="hfc-open-all" role="button" tabindex="0">Open All</span><span class="hfc-close-all" role="button" tabindex="0">Close All</span></div>');

    openCloseButtons.insertBefore('.small-only .field--name-field-collapsible-title:first');
    openCloseButtons.insertBefore('.large-only .field--name-field-collapsible-title:first');

    /* Open/close all buttons */
      $('.hfc-open-all').click(function(){
        $('.node__content__collapsibile').css('display','block');
        if ($(collapseTitle).hasClass('expand-plus')){
          $(collapseTitle).addClass('collapse-minus');
          $(collapseTitle).removeClass('expand-plus');
        }
        if ($('.node__content__collapsibile').hasClass('expand-arrow')){
          $('.node__content__collapsibile').addClass('collapse-arrow');
          $('.node__content__collapsibile').removeClass('expand-arrow');
        }
        $('.field--name-field-collapsible-title').attr('aria-expanded', 'true');
      })

      $('.hfc-close-all').click(function(){
        $('.node__content__collapsibile').css('display','none');
        if ($(collapseTitle).hasClass('collapse-minus')){
          $(collapseTitle).addClass('expand-plus');
          $(collapseTitle).removeClass('collapse-minus');
        }
        if ($('.node__content__collapsibile').hasClass('collapse-arrow')){
          $('.node__content__collapsibile').addClass('expand-arrow');
          $('.node__content__collapsibile').removeClass('collapse-arrow');
        }
        $('.field--name-field-collapsible-title').attr('aria-expanded', 'false');
      })

      /* change aria-expanded and toggle classes/display when clicked */
      $(collapseTitle).click(function(){
        if ($(this).hasClass('expand-plus')){
          $(this).toggleClass('expand-plus collapse-minus');
          $(this).next('.node__content__collapsibile').css('display','block');
        }

        else {
          $(this).toggleClass('expand-plus collapse-minus');
          $(this).next('.node__content__collapsibile').css('display','none');
        }

        $(this).attr('aria-expanded', function (i, attr) {
          return attr == 'true' ? 'false' : 'true';
        });
      })

      /* Keyboard events open/close all buttons */
      $('.hfc-open-all').keypress(function(evt){
        var e = evt || event;
        var code = e.keyCode || e.which;

        if (e.which === 13 || e.which === 32 || e.keyCode === 13 || e.keyCode === 32) {
          $('.node__content__collapsibile').css('display','block');
          if ($(collapseTitle).hasClass('expand-plus')){
            $(collapseTitle).addClass('collapse-minus');
            $(collapseTitle).removeClass('expand-plus');
          }
          if ($('.node__content__collapsibile').hasClass('expand-arrow')){
            $('.node__content__collapsibile').addClass('collapse-arrow');
            $('.node__content__collapsibile').removeClass('expand-arrow');
          }
          return false;
        }
      })

      $('.hfc-close-all').keypress(function(evt){
        var e = evt || event;
        var code = e.keyCode || e.which;

        if (e.which === 13 || e.which === 32 || e.keyCode === 13 || e.keyCode === 32) {
          $('.node__content__collapsibile').css('display','none');
          if ($(collapseTitle).hasClass('collapse-minus')){
            $(collapseTitle).addClass('expand-plus');
            $(collapseTitle).removeClass('collapse-minus');
          }
          if ($('.node__content__collapsibile').hasClass('collapse-arrow')){
            $('.node__content__collapsibile').addClass('expand-arrow');
            $('.node__content__collapsibile').removeClass('collapse-arrow');
          }
          return false;
        }
      })

    /* Keyboard events - show/hide content */
      $(collapseTitle).keypress(function(evt){
        var e = evt || event;
        var code = e.keyCode || e.which;

        if (e.which === 13 || e.which === 32 || e.keyCode === 13 || e.keyCode === 32) {
          if ($(this).hasClass('expand-plus')){
            $(this).toggleClass('expand-plus collapse-minus');
            $(this).next('.node__content__collapsibile').css('display','block');
          }

          else {
            $(this).toggleClass('expand-plus collapse-minus');
            $(this).next('.node__content__collapsibile').css('display','none');
            $(this).next('.node__content__collapsibile').css('display','none');
          }

          $(this).attr('aria-expanded', function (i, attr) {
            return attr == 'true' ? 'false' : 'true';
          });

          return false;
        }
      })
  })
